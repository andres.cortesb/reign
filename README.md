# Proyect Reign

### Prerequisites

    - Docker
    - Docker Compose

### 1. Installation

    - Clone the repository: git clone https://gitlab.com/andres.cortesb/reign.git

### 2. Initialize 

    - Open your terminal
    - Go to folder where do you have the Proyect
    -  run the following command: docker-compose build

### 3. Execution

    - Open your terminal
    - Go to folder where do you have the Proyect
    - run the following command : docker-compose Up 
    - Open your brwoser http://localhost:8000


