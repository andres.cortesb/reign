import * as mongoose from 'mongoose';
  
export class CreateDto extends mongoose.Document{
    readonly story_id: number
    readonly story_title: string
    readonly author: string    
    readonly story_url : string
    readonly created_at: string
    readonly disabled: boolean
    
}