import { Controller, Get, Param, Delete } from '@nestjs/common';
import { ArticlesService } from './articles.service'
import { ArticleI } from './interface/article.interface' 

@Controller('articles')
export class ArticlesController {

    constructor( private readonly ArticlesService: ArticlesService){}

    @Get()   
    getAllArticles(): Promise <ArticleI[]> {
        return this.ArticlesService.getall();
    }

    @Delete(':id')   
    DeleteArticle(@Param('id') idArticle: number): Promise <ArticleI> {
        return this.ArticlesService.DeleteArticle(idArticle);
    }

}