import * as mongoose from 'mongoose';

export interface ArticleI extends mongoose.Document{    
    story_id: number;
    story_title: string;
    author: string;
    story_url : string;
    created_at: string;
    disabled : boolean
}