import { Injectable } from '@nestjs/common';
import { ArticleI } from './interface/article.interface'
import { Model } from 'mongoose'
import { InjectModel } from '@nestjs/mongoose'

@Injectable()
export class ArticlesService {

    constructor(@InjectModel('articles') private readonly ArticleModel: Model<ArticleI>){}

    async  getall(): Promise<ArticleI[]>{
        return await this.ArticleModel.find({disabled: false}).sort({'created_at' : -1});
    }

    async DeleteArticle(idArticle: number): Promise<ArticleI> {
        return await this.ArticleModel.updateOne({ story_id : Number(idArticle) }, { disabled : true })
    }

}
