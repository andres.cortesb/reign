import * as mongoose from 'mongoose';

export const ArticleSchema = new mongoose.Schema({
    story_id: Number,
    story_title: String,
    author: String,
    story_url : String,
    created_at: String,
    disabled: Boolean    
});