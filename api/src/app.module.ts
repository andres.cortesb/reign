import { Module, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ArticlesController } from './articles/articles.controller';
import { ArticlesService } from './articles/articles.service';
import { MongooseModule } from '@nestjs/mongoose'
import { ArticleSchema } from './articles/schemas/article.schema'
import { ScheduleModule } from '@nestjs/schedule';
import { TasksController } from './tasks/tasks.controller';
import { TasksService } from './tasks/tasks.service';

@Module({
  imports: [ 
    MongooseModule.forRoot('mongodb://mongodatabase/reigndb'),
    MongooseModule.forFeature([{name : 'articles', schema: ArticleSchema }]),
    ScheduleModule.forRoot(),
    HttpModule
  ],
  controllers: [AppController, ArticlesController, TasksController],
  providers: [AppService, ArticlesService, TasksService]
})
export class AppModule {}
