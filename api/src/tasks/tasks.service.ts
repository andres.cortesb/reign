import { Injectable, Logger, HttpService } from '@nestjs/common';
import { map } from 'rxjs/operators';
import { Cron } from '@nestjs/schedule';
import { ArticleI } from '../articles/interface/article.interface'
import { Model } from 'mongoose'
import { InjectModel } from '@nestjs/mongoose'

@Injectable()
export class TasksService {

    constructor(
        @InjectModel('articles') private readonly ArticleModel: Model<ArticleI>, 
        private readonly httpService: HttpService
    ){}
    private readonly logger = new Logger(TasksService.name);
    
    @Cron('0 */1 * * * *')
    async handleCron() {
        let ArryArticles = []
        let ArryArticlesApi = []

        let allArticles = await this.ArticleModel.find({}, { story_id: 1, _id: 0});
        
        for (let articleA of allArticles) {
            ArryArticles.push(articleA.story_id)
        }
        
        let articles = await this.findAll()
        for (let article of articles) {
            if (article.story_id){
                
                article['disabled'] = false
                
                if (!ArryArticles.includes(article.story_id) && !ArryArticlesApi.includes(article.story_id)) {
                    const newArticle = new this.ArticleModel(article)
                    await newArticle.save()        
                }
                
                ArryArticlesApi.push(article.story_id)
            }
        }
    }
    
    async findAll() : Promise <any>{
        const myTodo = await this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs').pipe(
          map(resp => resp.data['hits']),
        ).toPromise();
        return myTodo;
    }
   
}
