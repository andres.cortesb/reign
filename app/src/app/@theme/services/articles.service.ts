import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { GetListArticles } from '../models';

@Injectable({ providedIn: 'root' })

export class ArticlesService {

    constructor(private http: HttpClient) {}

    getListArticles = (): Promise<GetListArticles> => {

        let promise = new Promise<GetListArticles>((resolve, reject) => {
    
          this.http.get(environment.apiURI+'articles')
          .toPromise()
          .then(
            (response) => {
              resolve(response as GetListArticles)
            },
            (error) => {
              reject(error);
            }
          )
    
        })
    
        return promise;
    
    }

    DelArticle = (id : string): Promise<any> => {

      let promise = new Promise<any>((resolve, reject) => {
  
        this.http.delete(environment.apiURI+'articles/'+id)
        .toPromise()
        .then(
          (response) => {
            resolve(response)
          },
          (error) => {
            reject(error);
          }
        )
  
      })
  
      return promise;
  
    }
  
  }