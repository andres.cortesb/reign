import { Component, OnInit } from '@angular/core';

import { ArticlesService } from '../../services';

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})
export class AdminLayoutComponent implements OnInit {

  list: any;
   
  constructor(
    private _Articles : ArticlesService
  ) {}

  ngOnInit() {

    this.getListArticles()
    
  }

  getListArticles(){

    this._Articles.getListArticles().then( (response) => {      
      this.list = response;      
      this.list.forEach(element => {
        element.style = true
      });
    }, (error) => {
      alert("Error: " + error.statusText)
    });
    
  }  

  deletearticle(id){
    
    this._Articles.DelArticle(id).then( (response) => {      
      if(response.nModified == '1'){
        this.getListArticles()
      }else{
        alert("The Article could not be deleted")
      }
    }, (error) => {
      alert("Error: " + error.statusText)
    });

  }

  changecoloron(item){
    item.style= false    
  }

  changecolorleave(item){
    item.style= true
  }  

}
