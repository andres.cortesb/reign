import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AdminLayoutRoutingModule } from './admin-layout-routing.module';

@NgModule({
  imports: [
    CommonModule,
    AdminLayoutRoutingModule,
    RouterModule
  ],
  declarations: [
  ]
})

export class AdminLayoutModule {}
