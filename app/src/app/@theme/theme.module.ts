import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ModulesModule } from './modules/modules.module'
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ModulesModule
  ],
  exports: [],
  declarations: [
    AdminLayoutComponent
  ],
  entryComponents: [],
})

export class ThemeModule {
  static forRoot(): ModuleWithProviders<ThemeModule> {
    return {
      ngModule: ThemeModule
    };
  }
}
