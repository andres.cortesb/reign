export interface GetListArticles{
    "message" : string,
    "data": Array<
      {
        "id"              : number,
        "nombre"          : string,
        "numerodocumento" : number,
        "email"           : string,
        "disabled"        : string
      }
    >
  }
  