import { NgModule } from '@angular/core';
import { ExtraOptions, Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './@theme/layouts/admin-layout/admin-layout.component';

const routes: Routes = [{
  path: '',
  component: AdminLayoutComponent,
  },
  {
    path: '**',
    redirectTo: '/',
  }
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
